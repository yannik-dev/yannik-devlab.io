// setting players
const player = "O";
const computer = "X";


let board_full = false;

// creating board with an array - 9 fields
let play_board = ["", "", "", "", "", "", "", "", ""];

const board_container = document.querySelector(".play-area");

const winner = document.getElementById("winner");

// checking each place (empty or not); board full -> flag = true
check_board_complete = function() {
    let flag = true;
    play_board.forEach(element => {
        if (element != player && element != computer) {
            flag = false;
        }
    });
    board_full = flag;
};

// checking playboard
const check_line = (a, b, c) => {
    return (
        play_board[a] == play_board[b] &&
        play_board[b] == play_board[c] &&
        (play_board[a] == player || play_board[a] == computer)
    );
};

// math; checking if 3 in a row
const check_match = function() {
    for (i = 0; i < 9; i += 3) {
        if (check_line(i, i + 1, i + 2)) {
            return play_board[i];
        }
    }
    for (i = 0; i < 3; i++) {
        if (check_line(i, i + 3, i + 6)) {
            return play_board[i];
        }
    }
    if (check_line(0, 4, 8)) {
        return play_board[0];
    }
    if (check_line(2, 4, 6)) {
        return play_board[2];
    }
    return "";
};

// choosing winner
const check_for_winner = function() {
    let res = check_match()
    if (res == player) {
        winner.innerText = "Player wins!";
        winner.classList.add("playerWin");
        board_full = true
    } else if (res == computer) {
        winner.innerText = "Computer wins!";
        winner.classList.add("computerWin");
        board_full = true
    } else if (board_full) {
        winner.innerText = "Draw!";
        winner.classList.add("draw");
    }
};

// rendering the board
const render_board = function() {
    board_container.innerHTML = ""
    play_board.forEach((e, i) => {
        board_container.innerHTML += `<div id="block_${i}" class="block" onclick="addPlayerMove(${i})">${play_board[i]}</div>`
        if (e == player || e == computer) {
            document.querySelector(`#block_${i}`).classList.add("occupied");
        }
    });
};

// loop after each move
const game_loop = function() {
    render_board();
    check_board_complete();
    check_for_winner();
}

// Player move; only allowed on empty places
const addPlayerMove = e => {
    if (!board_full && play_board[e] == "") {
        play_board[e] = player;
        game_loop();
        addComputerMove();
    }
};

// Computer move; random place which is not empty (place 0-8)
const addComputerMove = function() {
    if (!board_full) {
        do {
            selected = Math.floor(Math.random() * 9);
        } while (play_board[selected] != "");
        play_board[selected] = computer;
        game_loop();
    }
};

// reset board
const reset_board = function() {
    play_board = ["", "", "", "", "", "", "", "", ""];
    board_full = false;
    winner.classList.remove("playerWin");
    winner.classList.remove("computerWin");
    winner.classList.remove("draw");
    winner.innerHTML = "<br>";
    render_board();
};

//initial render
render_board();