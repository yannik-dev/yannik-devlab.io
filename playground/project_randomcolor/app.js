(function() {
    const button = document.querySelector('#generate') // select button
    const container = document.querySelector('#container') // select div #container
    const hexValues = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F'] // array with possible characters of a hex color
    const value = document.querySelector('#hex-value') // select hex text field

    button.addEventListener('click', changeColor) // change color on click

    function changeColor(){
        let hex = '#'

        for ( let i = 0; i < 6; i++ ){ // loop for 6 characters
            let index = Math.floor(Math.random()*hexValues.length) // choose six random characters from array
            hex += hexValues[index] // # + index => # - #F - #F2 - #F20 - #F202 - #F2025 - #F2025F
        }
        value.textContent = hex; // change hex code to new color
        container.style.backgroundColor = hex; // change background to new color
    }
} )()
document.getElementById("copy").addEventListener("click", copy); // select copy div; add event onclick function copy

function copy() {
    let copyText = document.getElementById("hex-value"); // select hex-value; create textarea to copy the text; remove textarea;
    let textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
}
