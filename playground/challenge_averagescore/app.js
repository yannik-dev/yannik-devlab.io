// Coding Challenge 2

// Score Calculator

let johnTeam = [ 89, 120, 103 ];
let markTeam = [ 116, 94, 123 ];
let maryTeam = [ 97, 134, 105 ];

function averageScore( team ) {
    let sum = 0;
    for ( e of team ) {
        sum = sum + e;
    }
    return sum / team.length;
}

let johnTeamAS = averageScore( johnTeam );
let markTeamAS = averageScore( markTeam );
let maryTeamAS = averageScore( maryTeam );

function winner() {
    if ( johnTeamAS > markTeamAS && johnTeamAS > maryTeamAS ) {
        return `John\s Team has the highest average score: ${johnTeamAS}`;
    } else if ( markTeamAS > johnTeamAS && markTeamAS > maryTeamAS ) {
        return `Mark\s Team has the highest average score: ${markTeamAS}`;
    } else if ( maryTeamAS > johnTeamAS && maryTeamAS > markTeamAS ) {
        return `Mary\s Team has the highest average score: ${maryTeamAS}`;
    } else if ( johnTeamAS === markTeamAS && johnTeamAS > maryTeamAS ) {
        return `John\s Team and Mark\s Team has the same average score:  ${johnTeamAS}`;
    } else if ( johnTeamAS === maryTeamAS && johnTeamAS > markTeamAS ) {
        return `John\s Team and Mary\s Team has the same average score:  ${johnTeamAS}`;
    } else if ( markTeamAS === maryTeamAS && markTeamAS > johnTeamAS ) {
        return `Mark\s Team and Mary\s Team has the same average score: ${markTeamAS}`;
    } else {
        return `All Teams have the same average score: ` + johnTeamAS;
    }
}

console.log(winner());