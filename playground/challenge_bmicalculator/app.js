// Coding Challenge 1

// BMI Calculator

let markHeight = 1.88;
let markMass = 82;

let johnHeight = 1.95;
let johnMass = 89;

let markBmi = markMass / ( markHeight * markHeight );
let johnBmi = johnMass / ( johnHeight * johnHeight );

let markHigherBmi = markBmi > johnBmi;

console.log( `Mark\s BMI: ${markBmi}; John\s BMI: ${johnBmi}` );
console.log( `Is Mark\s BMI higher than John\s? ${markHigherBmi}` );