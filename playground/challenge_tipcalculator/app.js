// Coding Challenge 3

// Tip Calculator

let bills = [ 124, 48, 268 ];

function percentages( bill ) {
    let percentage = 0;
    if ( bill < 50 ) {
        percentage = 0.2;
    } else if ( bill >= 50 && bill <= 200 ) {
        percentage = 0.15;
    } else {
        percentage = 0.1;
    }
    return percentage * bill;
}

let tips = [ percentages(bills[0]), percentages(bills[1]), percentages(bills[2]) ];
let paid = [ bills[0] + tips[0], bills[1] + tips[1], bills[2] + tips[2] ];

console.log( `bills: ${bills} \ntips: ${tips} \npaid: ${paid}` );