let todoItems = []; // todo array

const form = document.querySelector(".js-form"); // select form
form.addEventListener("submit", event => { // stop submit prevent reload
  event.preventDefault();
  const input = document.querySelector(".js-todo-input"); // select input
  const text = input.value.trim(); // remove whitespaces from input
  if (text !== "") { // if input not empty add text to todo, clear input, focus on input field
    addTodo(text);
    input.value = "";
    input.focus();
  }
});

function addTodo(text) { 
  // set text, checked status and js clock in milliseconds to const
  const todo = { 
    text,
    checked: false,
    id: Date.now()
  };

  todoItems.push(todo); // add object to array
  // add new list item 
  const list = document.querySelector(".js-todo-list");
  list.insertAdjacentHTML(
    "beforeend",
    `
    <li class="todo-item" data-key="${todo.id}">
      <input id="${todo.id}" type="checkbox"/>
      <label for="${todo.id}" class="tick js-tick"></label>
      <span>${todo.text}</span>
      <button class="delete-todo js-delete-todo">
        <svg><use href="#delete-icon"></use></svg>
      </button>
    </li>
  `
  );
}


// add event; if contains js-tick -> toggleDone: if js-delete-todo -> deleteTodo
const list = document.querySelector(".js-todo-list");
list.addEventListener("click", event => {
  if (event.target.classList.contains("js-tick")) {
    const itemKey = event.target.parentElement.dataset.key;
    toggleDone(itemKey);
  }

  if (event.target.classList.contains("js-delete-todo")) {
    const itemKey = event.target.parentElement.dataset.key;
    deleteTodo(itemKey);
  }
});


// function to add or remove class done
function toggleDone(key) {
  const index = todoItems.findIndex(item => item.id === Number(key));
  todoItems[index].checked = !todoItems[index].checked;

  const item = document.querySelector(`[data-key='${key}']`);
  if (todoItems[index].checked) {
    item.classList.add("done");
  } else {
    item.classList.remove("done");
  }
}

// select clicked item, remove object from array, 
function deleteTodo(key) {
  todoItems = todoItems.filter(item => item.id !== Number(key));
  const item = document.querySelector(`[data-key='${key}']`);
  item.remove();
  // clear list if array is empty
  const list = document.querySelector(".js-todo-list");
  if (todoItems.length === 0) list.innerHTML = "";
}