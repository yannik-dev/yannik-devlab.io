// Coding Challenge 4

// BMI Calculator

let john = {
    fullName: 'John Smith',
    mass: 82,
    height: 1.88,
    bmiCalculator: function() {
        this.bmi = this.mass / ( this.height * this.height );
        return this.bmi;
    }
};

let mark = {
    fullName: 'Mark Smith',
    mass: 97,
    height: 1.98,
    bmiCalculator: function() {
        this.bmi = this.mass / ( this.height * this.height );
        return this.bmi;
    }
};

if ( john.bmiCalculator() > mark.bmiCalculator() ) { 
    console.log( `${john.fullName} has the highest bmi: ${john.bmi}` );
} else if ( john.bmi < mark.bmi ) { 
    console.log( `${mark.fullName} has the highest bmi: ${mark.bmi}` );
} else {
    console.log( `${john.fullName} and ${mark.fullName} have the same bmi: ${john.bmi}` );
};