$(document).mousemove(function(e) {
    $('.pointer').css({ left: e.pageX, top: e.pageY });
})

var text = ["javascript hero", "website coder", "e-mail specialist","front-end-developer"];
var counter = 0;
var elem = $("#subheadline");
setInterval(change, 2500);
function change() {
    elem.fadeOut(function(){
        elem.html(text[counter]);
        counter++;
        if(counter >= text.length) { counter = 0; }
        elem.fadeIn();
    });
}

window.onblur=function(){
    $("#favicon").attr("href", "assets/faviconjs.ico");
}
window.onfocus=function(){
    $("#favicon").attr("href", "assets/favicon.ico");
}