// Berichtsheft

$(document).ready(function () {
	let counter = 2;
	$("#addButton").click(function () {
		if (counter > 10) {
			return false;
		} else {
		let newTextBoxDiv = $(document.createElement('div'))
		.attr("id", 'TextBoxDiv' + counter);

		newTextBoxDiv.after().html('<div class="form-group col-xs-8 col-sm-9 col-md-10"><input type="text" name="textbox[]" id="textbox' + counter + '" value="" placeholder="Tätigkeit ' + counter +
			'" class="form-control"></div><div class="form-group col-xs-4 col-sm-3 col-md-2"> <input type="number" name="time[]" id="textbox' + counter + 'h" value="" placeholder="Zeit" class="form-control" step="0.5" min="0"></div>');
		newTextBoxDiv.appendTo("#TextBoxesGroup");
		counter++;
		}
	});
	$("#removeButton").click(function () {
		if (counter == 2) {
			return false;
		} else {
		counter--;
		$("#TextBoxDiv" + counter).remove();
		}
	});
});

// date

let today = new Date();
let dd = String(today.getDate()).padStart(2, '0');
let mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
let yyyy = today.getFullYear();

today = dd + '/' + mm + '/' + yyyy;
document.getElementById("date").value = today;