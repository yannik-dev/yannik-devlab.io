const container = document.getElementById('container'); // select element with id="container"
const colors = ['#e74c3c', '#8e44ad', '#3498db', '#e67e22', '#2ecc71'] // array with colors

// for loop to create 400 divs with class square and add mouseover mouseout event
for( let i = 0; i < 400; i++ ) {
	
	let square = document.createElement('div'); // create new div
	
	square.classList.add('square'); // add class square to new div

	square.addEventListener('mouseover', function() { // add mouseover effect
		setColor(square);
	});

	square.addEventListener('mouseout', () => { // add mouseout effect (short function)
		removeColor(square);
	});
	container.appendChild(square); // append square to container div
}

// function to choose a random color from the array e.g. 0.333 * 4 = 1,332 -> 1 -> color #8e44ad
function getRandomColor() {
	return colors[Math.floor(Math.random() * colors.length)];
}

// function to set the random color to background
function setColor(element) {
	let color = getRandomColor();
	element.style.background = color;
	element.style.boxShadow = `0 0 2px 0 ${color}, 0 0 10px 0 ${color}`;
}

// function to set the color back to basic after mouseout
function removeColor(element) {
	element.style.background = '#1d1d1d';
	element.style.boxShadow = `0 0 2px 0 #000000`;
}
