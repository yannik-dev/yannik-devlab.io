const container = document.querySelector('#container'); // select container div
const unsplashURL = 'https://source.unsplash.com/random/'; // set unsplash url

for ( let i = 0; i < 9; i++ ) { // loop for 9 images
    let img = document.createElement('img'); // create img element
    img.src = `${unsplashURL}${getRandomSize()}`; // load random images e.g. https://source.unsplash.com/random/503x505
    container.appendChild(img); // append img to container div
}

function getRandomSize() { // return two random numbers between 500 and 509
    return `${getRandomNr()}x${getRandomNr()}`; // e.g. 506x501
}

function getRandomNr() { // generate random number e.g. 0.333 x 10 + 500 -> 503,33 -> 503
    return Math.floor(Math.random() * 10) + 500;
}